package com.example.blog.post;


import com.netflix.discovery.EurekaClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;


@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@EnableCircuitBreaker
public class PostApp implements CommandLineRunner{

    @Autowired
    private EurekaClient discoveryClient;

    @Override
    public void run(String... args) throws Exception {
//        InstanceInfo instance=
//                discoveryClient.getNextServerFromEureka(null,false);
//        System.out.println(instance.toString());
    }

    public static void main(String[] args){
        SpringApplication.run(PostApp.class,args);
    }
}
