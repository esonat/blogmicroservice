package com.example.blog.post.domain.model.entity;


import java.util.Date;

public class Post extends BaseEntity<Integer>{
    private String text;
    private Date datetime;
    private Integer userId;

    public Post(Integer id){
        super(id);
    }

    public Post(Integer id,String text,Integer userId,Date datetime){
        super(id);
        this.text=text;
        this.userId=userId;
        this.datetime=datetime;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }


    @Override
    public String toString() {
        return new
                StringBuilder("{id: ").append(id)
                .append(", text: ").append(text)
                .append(", userId: ").append(userId)
                .append("}").toString();
    }
}
