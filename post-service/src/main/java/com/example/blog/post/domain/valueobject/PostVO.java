package com.example.blog.post.domain.valueobject;

import java.util.Date;

public class PostVO {
    private Integer id;
    private Integer userId;
    private String text;
    private Date datetime;
    //private List<Comment> comments=new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

//    public List<Comment> getComments() {
//        return comments;
//    }
//
//    public void setComments(List<Comment> comments) {
//        this.comments = comments;
//    }

    @Override
    public String toString() {
        return new
                StringBuilder("{id: ").append(id)
                .append(", text: ").append(text)
                .append(", datetime: ").append(datetime)
                .append(", userId: ").append(userId)
                //.append(", comments: ").append(comments)
                .append("}").toString();
    }
}
