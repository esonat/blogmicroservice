package com.example.blog.post.domain.service;

import com.example.blog.post.domain.model.entity.Post;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by sonat on 23.09.2016.
 */
public interface PostService {

    void add(Post post) throws Exception;

    void update(Post post) throws Exception;

    void delete(Integer id) throws Exception;

    Post findById(Integer id) throws Exception;

    Collection<Post> findAll() throws Exception;

    List<Post> findPostsByUserId(Integer userId) throws Exception;

    List<Post> findByCriteria(Map<String, ArrayList<String>> name) throws Exception;

    Integer findLastId() throws Exception;
}

