package com.example.blog.post.domain.service;

import com.example.blog.post.domain.repository.Repository;

/**
 * Created by sonat on 23.09.2016.
 */


public abstract class ReadOnlyBaseService<TE,T> {
    private Repository<TE,T> repository;

    ReadOnlyBaseService(Repository<TE,T> repository){
        this.repository=repository;
    }
}
