package com.example.blog.post.resources;


import com.example.blog.post.domain.model.entity.Post;
import com.example.blog.post.domain.service.PostService;
import com.example.blog.post.domain.valueobject.CommentVO;
import com.example.blog.post.domain.valueobject.PostVO;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/v1/posts")
public class PostController {
    protected static final Logger logger = LoggerFactory.getLogger(PostController.class.getName());

    protected PostService postService;

    @Autowired
    public PostController(PostService postService) {
        this.postService=postService;
    }

    @FeignClient("comment-service")
    interface CommentClient {
        @RequestMapping(method = RequestMethod.POST,value="/v1/comments")
        String add(@RequestBody CommentVO commentVO);
    }

    @Autowired
    CommentClient commentClient;

    @HystrixCommand(fallbackMethod = "defaultPosts")
    @RequestMapping(method= RequestMethod.GET)
    public ResponseEntity<List<Post>> findAllPosts(@RequestParam(value = "userId",required = false)Integer userId){
        List<Post> posts=new ArrayList<>();
        try{
            if(userId==null){
                posts.addAll(postService.findAll());
            }else if(userId!=null){
                posts.addAll(postService.findPostsByUserId(userId));
            }
        }catch (Exception e){
            logger.error("Exception in findAllPosts method");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return posts.size()>0? new ResponseEntity<>(posts,HttpStatus.OK)
                :new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @HystrixCommand(fallbackMethod = "defaultPost")
    @RequestMapping(value="/{post_id}",method = RequestMethod.GET)
    public ResponseEntity<Post> findById(@PathVariable("post_id")int id){
        Post post;
        try{
            post=postService.findById(id);
        }catch (Exception e){
            return new ResponseEntity<Post>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return post!=null ? new ResponseEntity<>(post,HttpStatus.OK)
                :new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }


    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity add(@RequestBody PostVO postVO){
        Post post;

        try {
            post = new Post(postService.findLastId());
        }catch (Exception e) {
            return new ResponseEntity("Exception finding last id",HttpStatus.INTERNAL_SERVER_ERROR);
        }

        try {
            //BeanUtils.copyProperties(postVO,post);
            post.setText(postVO.getText());
            post.setUserId(postVO.getUserId());
            post.setDatetime(new Date());

            postService.add(post);
            /*
                ADD COMMENT
             */

            CommentVO commentVO=new CommentVO(1,post.getId(),null,"FIRST COMMENT!",new Date(),0);
            this.commentClient.add(commentVO);

            /* */

        }catch (Exception e){
            return new ResponseEntity("Exception adding post",HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }


    @RequestMapping(value = "/{postId}",method = RequestMethod.DELETE)
    public ResponseEntity<String> deletePost(@PathVariable("postId")Integer postId){
        try {
            postService.delete(postId);
        }catch (Exception e){
            logger.error("Exception while deleting post");
            return new ResponseEntity<String>("Exception while deleting post",HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<String>("Deleted",HttpStatus.NO_CONTENT);
    }

    public ResponseEntity<List<Post>> defaultPosts(Integer userId){
        logger.error("Fallback method for posts-service is being used");
        return new ResponseEntity(null,HttpStatus.NO_CONTENT);
    }

    public ResponseEntity<Post> defaultPost(int id){
        logger.error("Fallback method for posts-service is being used");
        return new ResponseEntity(null,HttpStatus.NO_CONTENT);
    }
}
