package com.example.blog.post.domain.repository;

import com.example.blog.post.domain.model.entity.Entity;

import java.util.Collection;

/**
 * Created by sonat on 23.09.2016.
 */
public interface ReadOnlyRepository<TE,T> {
     boolean contains(T id);

    Entity get(T id);

    Collection<TE> getAll();
}
