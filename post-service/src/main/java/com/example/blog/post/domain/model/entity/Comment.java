package com.example.blog.post.domain.model.entity;



public class Comment extends BaseEntity<Integer>{

    private String text;
    private int userId;
    private int postId;


    public Comment(int id){
        super(id);
    }

    public Comment(int id,String text,int userId,int postId){
        super(id);
        this.text=text;
        this.userId=userId;
        this.postId=postId;
    }


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    @Override
    public String toString() {
        return new
                StringBuilder("{id: ").append(id)
                .append(", text: ").append(text)
                .append(", userId: ").append(userId)
                .append(", postId: ").append(postId)
                .append("}").toString();
    }
}
