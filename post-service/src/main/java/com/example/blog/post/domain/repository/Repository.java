package com.example.blog.post.domain.repository;

/**
 * Created by sonat on 23.09.2016.
 */
public interface Repository<TE,T> extends ReadOnlyRepository<TE,T> {

    void add(TE entity);

    void remove(T id);

    void update(TE entity);
}
