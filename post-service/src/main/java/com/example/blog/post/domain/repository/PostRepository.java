package com.example.blog.post.domain.repository;

import java.util.List;

/**
 * Created by sonat on 23.09.2016.
 */
public interface PostRepository<Post,Integer> extends Repository<Post,Integer>{

    boolean containsText(String text);

    List<Post> findPostsByUserId(Integer userId) throws Exception;

    Integer findLastId() throws Exception;

    Post findById(Integer id) throws Exception;
  //  public Collection<Comment> findCommentsById(int id) throws Exception;
}
