package com.example.blog.post.domain.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by sonat on 23.09.2016.
 */
public abstract class BaseEntity<T> extends Entity<T> {
    @JsonIgnore
    private boolean isModified;

    public BaseEntity(T id) {
        super.id = id;
        isModified = false;
    }

    public boolean isIsModified() {
        return isModified;
    }
}
