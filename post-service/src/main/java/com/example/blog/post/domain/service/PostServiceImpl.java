package com.example.blog.post.domain.service;


import com.example.blog.post.domain.model.entity.Post;
import com.example.blog.post.domain.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Service("postService")
public class PostServiceImpl extends BaseService<Post,Integer>
 implements PostService{

    private PostRepository<Post,Integer> postRepository;

    @Autowired
    public PostServiceImpl(PostRepository<Post,Integer> postRepository){
        super(postRepository);
        this.postRepository=postRepository;
    }

    @Override
    public void add(Post entity) throws Exception {
        if(postRepository.containsText(entity.getText())){
            throw new Exception(String.format("There is already a post with the same text - %s",entity.getText()));
        }
        if(entity.getText()==null || "".equals(entity.getText())){
            throw new Exception("Post text cannot be null or empty");
        }

        postRepository.add(entity);
    }

    @Override
    public void update(Post post) throws Exception {
        postRepository.update(post);
    }

    @Override
    public void delete(Integer id) throws Exception {
        postRepository.remove(id);
    }

    @Override
    public Post findById(Integer id) throws Exception {
        return postRepository.findById(id);
    }

    @Override
    public List<Post> findByCriteria(Map<String, ArrayList<String>> name) throws Exception {
        return null;
    }

    @Override
    public Collection<Post> findAll() {
        return postRepository.getAll();
    }

    @Override
    public List<Post> findPostsByUserId(Integer userId) throws Exception {
        return postRepository.findPostsByUserId(userId);
    }

    @Override
    public Integer findLastId() throws Exception {
        return postRepository.findLastId();
    }
}
