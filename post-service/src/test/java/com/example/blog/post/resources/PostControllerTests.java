package com.example.blog.post.resources;

import com.example.blog.post.domain.model.entity.Entity;
import com.example.blog.post.domain.model.entity.Post;
import com.example.blog.post.domain.repository.PostRepository;
import com.example.blog.post.domain.service.PostService;
import com.example.blog.post.domain.service.PostServiceImpl;
import org.junit.Before;

import java.util.*;

public class PostControllerTests extends AbstractPostControllerTests{

    protected static class TestPostRepository implements PostRepository<Post,Integer>
    {
        private Map<Integer,Post> posts;

        public TestPostRepository(){
            posts=new HashMap<>();
            Post post=new Post(1,  "post1",1,new Date());
            Post post2=new Post(2, "post2",1,new Date());

            posts.put(1,post);
            posts.put(2,post2);
        }

        @Override
        public boolean containsText(String text) {
            for(Post post:posts.values()){
                if(text.equals(post.getText())) return true;
            }

            return false;
        }

//    @Override
//    public List<Comment> findCommentsById(int id){
//        return posts.get(id).getComments();
//    }

        @Override
        public void add(Post post){
            posts.put(post.getId(),post);
        }

        @Override
        public void remove(Integer id){
            if(posts.containsKey(id)){
                posts.remove(id);
            }
        }

        @Override
        public void update(Post post){
            if(posts.containsKey(post.getId())){
                posts.put(post.getId(),post);
            }
        }

        @Override
        public boolean contains(Integer id){
            return posts.containsKey(id);
        }

        @Override
        public Entity get(Integer id){
            return posts.get(id);
        }

        @Override
        public Collection<Post> getAll(){
            return posts.values();
        }

        @Override
        public List<Post> findPostsByUserId(Integer userId) throws Exception {
            List<Post> collection=new ArrayList<>();

            for(Post post:posts.values()){
                if(userId==post.getUserId()) collection.add(post);
            }
            return collection;
        }

        @Override
        public Integer findLastId() throws Exception {
            return posts.keySet().size()+1;
        }

        @Override
        public Post findById(Integer id) throws Exception {
            return posts.get(id);
        }
    }
    protected TestPostRepository testPostRepository=new TestPostRepository();
    protected PostService postService=new PostServiceImpl(testPostRepository);


    @Before
    public void setup(){
        postController=new PostController(postService);
    }

}
