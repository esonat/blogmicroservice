package com.example.blog.post.resources;

import com.example.blog.post.domain.model.entity.Post;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public abstract class AbstractPostControllerTests {

    protected static final Integer POST_ID=1;
    protected static final String POST_TEXT="post1";
    protected static Integer USER_ID=1;

    @Autowired
    PostController postController;

    @Test
    public void validPostById(){
        ResponseEntity<Post> post=postController.findById(POST_ID);

        assertEquals(HttpStatus.OK,post.getStatusCode());
        assertTrue(post.hasBody());
        assertNotNull(post.getBody());
        assertEquals(POST_ID,post.getBody().getId());
        assertEquals(POST_TEXT,post.getBody().getText());
    }

}
