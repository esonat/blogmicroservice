package com.example.blog.post.resources;


import com.example.blog.post.PostApp;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes=PostApp.class)
@WebIntegrationTest("server.port=0")
public class PostControllerIntegrationTests extends AbstractPostControllerTests{

    private final RestTemplate restTemplate=new TestRestTemplate();
    public static final ObjectMapper objectMapper=new ObjectMapper();

    @Value("${local.server.port}")
    private int port;

    @Test
    public void testGetById() {
        Map<String,Object> response=
                restTemplate.getForObject("http://localhost:"+port+"/v1/posts/1",Map.class);
        assertNotNull(response);

        String id=response.get("id").toString();
        assertNotNull(id);
        assertEquals("1",id);
    }

    @Test
    public void testGetById_NoContent(){
        HttpHeaders headers=new HttpHeaders();
        HttpEntity<Object> entity=new HttpEntity<>(headers);
        ResponseEntity<Map> responseE=restTemplate.exchange("http://localhost:"+port+"/v1/posts/99", HttpMethod.GET,entity,Map.class);


        assertNotNull(responseE);
        assertEquals(HttpStatus.NO_CONTENT,responseE.getStatusCode());
    }

    @Test
    public void testAdd() throws JsonProcessingException{
        Map<String,Object> requestBody=new HashMap<>();

        requestBody.put("id","3");
        requestBody.put("text","post3");
        requestBody.put("userId","1");

        HttpHeaders headers=new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity=new HttpEntity<>(objectMapper.writeValueAsString(requestBody),headers);

        ResponseEntity<Map> responseE=restTemplate.exchange("http://localhost:"+port+"/v1/posts",HttpMethod.POST,entity,Map.class, Collections.EMPTY_MAP);

        assertNotNull(responseE);

        assertEquals(HttpStatus.CREATED,responseE.getStatusCode());

        Map<String,Object> response=
                restTemplate.getForObject("http://localhost:"+port+"/v1/posts/3",Map.class);

        assertNotNull(response);

        String id=response.get("id").toString();
        assertNotNull(id);
        assertEquals("3",id);
        String text=response.get("text").toString();
        assertNotNull(text);
        assertEquals("post3",text);

    }
}
