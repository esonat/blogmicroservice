package com.example.blog.user.domain.service;

import com.example.blog.user.domain.model.entity.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface UserService {


    void add(User user) throws Exception;

    void update(User user) throws Exception;

    void delete(Integer id) throws Exception;

    User findById(Integer id) throws Exception;

    User findByUsername(String username) throws Exception;

    List<User> findAllUsers() throws Exception;

    Collection<User> findByCriteria(Map<String, ArrayList<String>> name) throws Exception;
}
