package com.example.blog.user.domain.repository;

/**
 * Created by sonat on 23.09.2016.
 */
public interface UserRepository<User,Integer> extends Repository<User,Integer>{
    boolean containsUsername(String username);

    public User findByUsername(String username) throws Exception;
}
