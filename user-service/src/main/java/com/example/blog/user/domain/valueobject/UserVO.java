package com.example.blog.user.domain.valueobject;

public class UserVO {
    private Integer id;
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new StringBuilder("{id: ").append(id)
                .append(", username: ").append(username)
                .append(", password: ").append(password)
                .append("}").toString();
    }
}
