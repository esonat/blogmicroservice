package com.example.blog.user.resources;


import com.example.blog.user.domain.model.entity.User;
import com.example.blog.user.domain.service.UserService;
import com.example.blog.user.domain.valueobject.UserVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/v1/users")
public class UserController {
    /**
     *
     */
    protected static final Logger logger = LoggerFactory.getLogger(UserController.class.getName());

    protected UserService userService;

    /**
     * @param userService
     */
    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }


    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<User>> findAllUsers(@RequestParam(value = "name",required = false)String username){
        List<User> users=new ArrayList<>();

        try{
            if(username==null){
                users=userService.findAllUsers();
            }else if(username!=null){
                User user=userService.findByUsername(username);
                users.add(user);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return users!=null ? new ResponseEntity<>(users,HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<User> findById(@PathVariable("id") Integer id) {
        User user;

        try {
            user = userService.findById(id);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return user != null ? new ResponseEntity<User>(user, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<User> add(@RequestBody UserVO userVO) {
        User user = new User(3,"deneme","deneme");
        BeanUtils.copyProperties(userVO, user);

        try {
            userService.add(user);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}