package com.example.blog.user.domain.repository;

import com.example.blog.user.domain.model.entity.User;

import java.util.Collection;

/**
 * Created by sonat on 23.09.2016.
 */
public interface ReadOnlyRepository<TE,T> {

    //long Count;
    /**
     *
     * @param id
     * @return
     */
    boolean contains(T id);

    /**
     *
     * @param id
     * @return
     */
    User get(T id);

    /**
     *
     * @return
     */
    Collection<TE> getAll();
}
