package com.example.blog.user.domain.service;

import com.example.blog.user.domain.model.entity.User;
import com.example.blog.user.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Service("userService")
public class UserServiceImpl extends BaseService<User,Integer> implements UserService{

    private UserRepository<User,Integer> userRepository;

    @Autowired
    public UserServiceImpl(UserRepository<User,Integer> userRepository){
        super(userRepository);
        this.userRepository=userRepository;
    }

    @Override
    public void add(User user) throws Exception{
        if(userRepository.containsUsername(user.getUsername())){
            throw new Exception("There is already a user with the same username");
        }

        if(user.getUsername()==null || "".equals(user.getUsername())){
            throw new Exception("Username cannot be null or empty");
        }
        super.add(user);
    }


    @Override
    public User findByUsername(String username) throws Exception{
        return userRepository.findByUsername(username);
    }

    @Override
    public void update(User user) throws Exception{
        userRepository.update(user);
    }

    @Override
    public void delete(Integer id) throws Exception{
        userRepository.remove(id);
    }

    @Override
    public User findById(Integer id) throws Exception{
        return userRepository.get(id);
    }

    @Override
    public List<User> findAllUsers() throws Exception{
        List<User> users=new ArrayList<>();
        users.addAll(userRepository.getAll());
        return users;
    }

    @Override
    public Collection<User> findByCriteria(Map<String,ArrayList<String>> name) throws Exception{
        return null;
    }
}
