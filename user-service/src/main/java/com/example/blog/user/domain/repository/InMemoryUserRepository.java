package com.example.blog.user.domain.repository;

import com.example.blog.user.domain.model.entity.User;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository("userRepository")
public class InMemoryUserRepository implements UserRepository<User,Integer>{
    private Map<Integer,User> users;

    public InMemoryUserRepository(){
        users=new HashMap<>();
        User user=new User(1,"user1","user1");
        users.put(1,user);
        User user2=new User(2,"user2","user2");
        users.put(2,user2);
    }

    @Override
    public boolean containsUsername(String username) {
        return users.values().stream().anyMatch(u->u.getUsername().equals(username));
    }

    @Override
    public User findByUsername(String username) throws Exception {
        for(User user:users.values()){
            if(user.getUsername().equals(username)) return user;
        }
        return null;
    }

    @Override
    public void add(User entity) {
        users.put(entity.getId(),entity);
    }

    @Override
    public void remove(Integer id) {
        if(users.containsKey(id)){
            users.remove(id);
        }
    }

    @Override
    public void update(User entity) {
        if(users.containsKey(entity.getId())){
            users.put(entity.getId(),entity);
        }
    }

    @Override
    public boolean contains(Integer id) {
        return users.containsKey(id);
    }

    @Override
    public User get(Integer id) {
        return users.get(id);
    }

    @Override
    public Collection<User> getAll() {
        return users.values();
    }
}
