package com.example.blog.user.domain.model.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;

public abstract class BaseEntity<T> extends Entity<T> {

    @JsonIgnore
    private boolean isModified;

    public BaseEntity(T id) {
        super.id = id;
        isModified = false;
    }

    /**
     *
     * @return
     */
    public boolean isIsModified() {
        return isModified;
    }

}
