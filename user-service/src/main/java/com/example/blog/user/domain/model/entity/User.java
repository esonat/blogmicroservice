package com.example.blog.user.domain.model.entity;

/**
 * Created by sonat on 23.09.2016.
 */
public class User extends BaseEntity<Integer>{

    private String username;
    private String password;

    public User(Integer id,String username,String password){
        super(id);
        this.username=username;
        this.password=password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return new StringBuilder("{id: ").append(id)
                .append(", username: ").append(username)
                .append(", password: ").append(password)
                .append("}").toString();
    }
}
