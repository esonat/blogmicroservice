package com.example.blog.zuul.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;


@SpringBootApplication
@EnableZuulProxy
@EnableEurekaClient
@EnableCircuitBreaker
@Configuration
@EnableFeignClients
public class EdgeApp {
    @LoadBalanced
    @Bean
    RestTemplate restTemplate(){
        return new RestTemplate();
    }

    private static final Logger LOG= LoggerFactory.getLogger(EdgeApp.class);

    @Value("${app.rabbitmq.host:localhost}")
    String rabbitMqHost;

    @Bean
    public ConnectionFactory connectionFactory(){
        LOG.info("Create RabbitMqCF for host: {}",rabbitMqHost);
        CachingConnectionFactory connectionFactory=
                new CachingConnectionFactory(rabbitMqHost);

        return connectionFactory;
    }

    public static void main(String[] args){
        SpringApplication.run(EdgeApp.class,args);
    }
}

//@Component
//class DiscoveryClientSample implements CommandLineRunner{
//    @Autowired
//    private DiscoveryClient discoveryClient;
//
//    @Override
//    public void run(String... strings) throws Exception{
//        System.out.println(discoveryClient.description());
//        discoveryClient.getInstances("post-service").forEach((ServiceInstance serviceInstance)->{
//            System.out.println("Instance --> "+serviceInstance.getServiceId()
//                    +"\nServer: "+serviceInstance.getHost()+":"+serviceInstance.getPort()
//                    +"\nURI:"+serviceInstance.getUri()+"\n\n\n");
//        });
//    }
//}
//
//@Component
//class RestTemplateExample implements CommandLineRunner {
//    @Autowired
//    private RestTemplate restTemplate;
//
//    @Override
//    public void run(String... strings) throws Exception {
//        System.out.println("\n\n\n start RestTemplate client...");
//        ResponseEntity<Collection<Post>> exchange
//                = this.restTemplate.exchange(
//                "http://post-service/v1/posts",
//                HttpMethod.GET,
//                null,
//                new ParameterizedTypeReference<Collection<Post>>() {
//                },
//                (Object) "posts");
//
//        exchange.getBody().forEach((Post post) -> {
//            System.out.println("\n\n\n[ " + post.getId() + " " + post.getText() + "]");
//        });
//    }
//}
//
//@FeignClient(name="post-service",url = "http://localhost:2222")
//interface PostClient{
//    @RequestMapping(method = RequestMethod.GET,value = "/v1/posts")
//    List<Post> getPosts();
//}
//
//@Component
//class FeignSample implements CommandLineRunner{
//    @Autowired
//    private PostClient postClient;
//
//    @Override
//    public void run(String... strings) throws Exception{
//        this.postClient.getPosts().forEach((Post post)->{
//            System.out.println("\n\n\n[ "+post.getId()+" "+post.getText()+"]");
//        });
//    }
//}
//
//class Post{
//    private Integer id;
//    private String text;
//    private Date datetime;
//    private Integer userId;
//
//    public Post(){}
//
//    public Integer getId() {
//        return id;
//    }
//
//    public void setId(Integer id) {
//        this.id = id;
//    }
//
//    public String getText() {
//        return text;
//    }
//
//    public void setText(String text) {
//        this.text = text;
//    }
//
//    public Date getDatetime() {
//        return datetime;
//    }
//
//    public void setDatetime(Date datetime) {
//        this.datetime = datetime;
//    }
//
//    public Integer getUserId() {
//        return userId;
//    }
//
//    public void setUserId(Integer userId) {
//        this.userId = userId;
//    }
//}