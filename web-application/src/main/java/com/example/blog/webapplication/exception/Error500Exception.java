package com.example.blog.webapplication.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.INTERNAL_SERVER_ERROR, reason="Internal Server Error")
public class Error500Exception extends RuntimeException{

    private static final long serialVersionUID =3935230281455340039L;
}
