package com.example.blog.webapplication.client;

import com.example.blog.webapplication.model.entity.Comment;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

public class Main {

    public static List<Comment> getChildComments(Integer parentId){
        RestTemplate restTemplate=new RestTemplate();
        List<LinkedHashMap<String,Object>> commentMap=restTemplate.getForObject("http://localhost:2225/v1/comments/"+parentId+"/children",List.class);
        List<Comment> children=new ArrayList<>();

        if(commentMap!=null){
            for(LinkedHashMap<String,Object> map:commentMap){
                Comment comment=new Comment((Integer)map.get("id"));
                comment.setText((String)map.get("text"));
                comment.setDatetime(new Date((Long)map.get("datetime")));
                comment.setParentId(parentId);
                comment.setPostId((Integer)map.get("postId"));
                comment.setDepth((Integer)map.get("depth"));
                children.add(comment);
            }
        }
        return children;
    }

    public static void main(String[] args){
        RestTemplate restTemplate=new RestTemplate();
        List<LinkedHashMap<String,Object>> commentsMap=restTemplate.getForObject("http://localhost:2225/v1/comments?postId=1",List.class);
        List<Comment> comments=new ArrayList<>();

        for(LinkedHashMap<String,Object> map:commentsMap){
            Integer commentId=(Integer)map.get("id");

            Comment comment=new Comment(commentId);
            if(map.get("postId")!=null) comment.setPostId((Integer)map.get("postId"));
            if(map.get("parentId")!=null) comment.setParentId(1);
            if(map.get("text")!=null) comment.setText((String)map.get("text"));
            if(map.get("datetime")!=null) comment.setDatetime(new Date());
            //comment.setChildren(getChildComments(commentId));
            //Comment comment=new Comment((int)map.get("id"),(int)map.get("postId"),(int)map.get("parentId"),(String)map.get("text"),(Date)map.get("datetime"));
            comments.add(comment);
        }

        for(Comment comment:comments){
            List<Comment> children=getChildComments(comment.getId());
            for(Comment child:children){
                for(Comment c:comments){
                    if(c.getId().equals(child.getId())) comment.getChildren().add(c);
                }
            }
        }

        List<Comment> children=new ArrayList<>();
        for(Comment c:comments){
            if(c.getId().equals(1)){
                for(Comment child:c.getChildren()){

                    if(child.getChildren()!=null) {
                        for (Comment item : child.getChildren()) {
                            System.out.println("Parent:" + item.getId() + "Text:" + item.getText());
                        }
                    }
                }
            }
        }

    }
}


