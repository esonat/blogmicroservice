package com.example.blog.webapplication.web;

import com.example.blog.webapplication.config.BeanConfiguration;
import com.example.blog.webapplication.config.WebConfiguration;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class WebApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer{

    @Override
    protected Class<?>[] getRootConfigClasses(){
        return new Class<?>[] {BeanConfiguration.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses(){
        return new Class<?>[]{WebConfiguration.class};
    }

    @Override
    protected String[] getServletMappings(){
        return new String[] {"/"};
    }
}
