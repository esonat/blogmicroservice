package com.example.blog.webapplication.converter;

import com.example.blog.webapplication.model.entity.User;
import com.example.blog.webapplication.valueobject.UserVO;
import org.dozer.CustomConverter;
import org.dozer.MappingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class UserConverter implements CustomConverter{

    private Logger logger= LoggerFactory.getLogger(UserConverter.class);

    public Object convert(Object destination,Object source,Class destClass,Class sourceClass){
        if(source==null){
            return null;
        }

        User dest=null;
        if(source instanceof Integer){
            if(destination==null){
                dest=new User(1);
            }else{
                dest=(User)destination;
            }

            RestTemplate restTemplate=new RestTemplate();
            ResponseEntity<UserVO> response=null;

            try {
                response = restTemplate.getForEntity("http://localhost:2224/v1/users/" + (Integer) source, UserVO.class);
            }catch (Exception e){
                logger.error("Exception while requesting user");
            }

            UserVO userVO=null;

            try {
                userVO = response.getBody();
            }catch (Exception e){
                logger.error("Exception in response body");
            }

            dest.setId((Integer)source);
            dest.setUsername(userVO.getUsername());
            dest.setPassword(userVO.getPassword());

            return dest;
        }else{
            throw new MappingException("Converter UserConverter used incorrectly.Arguments" +
                    "passed in were:"+destination+"and "+source);
        }
    }
}
