package com.example.blog.webapplication.model.entity;

import java.util.ArrayList;
import java.util.List;

public class User extends BaseEntity<Integer>{

    private String username;
    private String password;
    private List<Post> posts=new ArrayList<Post>();
    private List<Comment> comments=new ArrayList<Comment>();

    public User(Integer id){
        super(id);
    }

    public User(Integer id, String username, String password,List<Post> posts,List<Comment> comments){
        super(id);
        this.username=username;
        this.password=password;
        this.posts=posts;
        this.comments=comments;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        return new StringBuilder("{id: ").append(id)
                .append(", username: ").append(username)
                .append(", password: ").append(password)
                .append(", posts: ").append(posts)
                .append(", comments ").append(comments)
                .append("}").toString();
    }
}
