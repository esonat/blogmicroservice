package com.example.blog.webapplication.model.entity;

/**
 * Created by sonat on 23.09.2016.
 */
public abstract class BaseEntity<T> extends Entity<T> {
    private boolean isModified;

    public BaseEntity(T id) {
        super.id = id;
        isModified = false;
    }

    public boolean isIsModified() {
        return isModified;
    }
}
