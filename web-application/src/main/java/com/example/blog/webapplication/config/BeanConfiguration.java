package com.example.blog.webapplication.config;

import org.dozer.DozerBeanMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;

@Configuration
public class BeanConfiguration {

    @Bean(name="mapper")
    public DozerBeanMapper mapper(){
        DozerBeanMapper mapper=new DozerBeanMapper();
        List<String> list= Arrays.asList("dozer-bean-mappings.xml");
//"dozer-global-configuration.xml",

        mapper.setMappingFiles(list);
        return mapper;
    }


//    @Bean
//    public DozerBeanMapperFactoryBean dozerBeanMapperFactoryBean(){
//        DozerBeanMapperFactoryBean bean=new DozerBeanMapperFactoryBean();
//        bean.setMappingFiles(new Resource[]{
//                new ClassPathResource("classpath:/*mapping.xml")
//        });
//
//
//    }
}
