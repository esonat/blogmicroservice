package com.example.blog.webapplication.valueobject;

import java.util.Date;

public class PostVO {
    private Integer id;
    private Integer userId;
    private String text;
    private Date datetime;

    public PostVO(){
    }

    public PostVO(Integer id, Integer userId, String text, Date datetime) {
        this.id = id;
        this.userId = userId;
        this.text = text;
        this.datetime = datetime;
    }

    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }


    @Override
    public String toString() {
        return new
                StringBuilder("{id: ").append(id)
                .append(", text: ").append(text)
                .append(", datetime: ").append(datetime)
                .append(", userId: ").append(userId)
                .append("}").toString();
    }
}
