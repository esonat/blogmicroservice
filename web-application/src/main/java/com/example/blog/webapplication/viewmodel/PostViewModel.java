package com.example.blog.webapplication.viewmodel;

import com.example.blog.webapplication.model.entity.Post;

public class PostViewModel {
    private Post post;
    private String username;
    private String datetime;

    public PostViewModel(){}
    public PostViewModel(Post post, String username, String datetime) {
        this.post = post;
        this.username = username;
        this.datetime = datetime;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }
}
