//package com.example.blog.webapplication.config;
//
//import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
//import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
//import org.springframework.boot.context.embedded.ErrorPage;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.http.HttpStatus;
//
//@Configuration
//public class ErrorConfiguration implements EmbeddedServletContainerCustomizer {
//    @Override
//    public void customize(ConfigurableEmbeddedServletContainer container){
//        container.addErrorPages(new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR,"/error"));
//    }
//}
