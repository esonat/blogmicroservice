package com.example.blog.webapplication.controller;

import com.example.blog.webapplication.model.entity.Comment;
import com.example.blog.webapplication.model.entity.Post;
import com.example.blog.webapplication.model.entity.User;
import com.example.blog.webapplication.valueobject.CommentVO;
import com.example.blog.webapplication.valueobject.PostVO;
import com.example.blog.webapplication.valueobject.UserVO;
import com.example.blog.webapplication.viewmodel.PostViewModel;
import com.example.blog.webapplication.viewmodel.UserViewModel;
import org.dozer.DozerBeanMapper;
import org.dozer.MappingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class DefaultController {

    private Logger logger= LoggerFactory.getLogger(DefaultController.class);

//    public DefaultController(){
//        super();
//    }

    @Autowired
    private DozerBeanMapper mapper;

    //@Value("${post-service.url}")
    private String postservice_url="http://localhost:2222/v1/posts";

    //@Value("${comment-service.url}")
    private String commentservice_url="http://localhost:2225/v1/comments";

    //Value("${user-service.url}")
    private String userservice_url="http://localhost:2224/v1/users";


    public User getUserFromUserId(Integer userId)throws Exception{
        RestTemplate restTemplate=new RestTemplate();

        ResponseEntity<UserVO> response=restTemplate.getForEntity(userservice_url+"/"+userId,UserVO.class);
        UserVO userVO=response.getBody();

        User user=new User(userId);
        user.setPassword(userVO.getPassword());
        user.setUsername(userVO.getUsername());

        return user;
  }

    public List<Comment> getChildComments(Integer parentId) throws Exception{
        RestTemplate restTemplate=new RestTemplate();
        ResponseEntity<CommentVO[]> response=restTemplate.getForEntity(commentservice_url+"/"+parentId+"/children",CommentVO[].class);

        List<Comment> children=new ArrayList<>();

        for(CommentVO commentVO:response.getBody()){
            Comment comment=new Comment(commentVO.getId());
            mapper.map(commentVO,comment);
            children.add(comment);
        }

        return children;
    }


    public List<Comment> getCommentTree(int postId){
        return null;
    }

    @RequestMapping("/posts")
    public String findAllPosts(Model model){
        RestTemplate restTemplate=new RestTemplate();
        ResponseEntity<PostVO[]> response = restTemplate.getForEntity(postservice_url,PostVO[].class);
        List<Post> postList=new ArrayList<>();

        PostVO[] responseList=response.getBody();
        if(responseList==null || responseList.length==0)  return "posts";

        try {
            for (PostVO postVO : responseList) {

                Post post = new Post(postVO.getId());
                mapper.map(postVO, post);
                postList.add(post);
            }

        }catch (MappingException e){
            logger.error(e.getCause()+e.getMessage());
        }catch (Exception e){
            logger.error("Exception in findAllPosts method - Mapping posts");
        }

        List<Post>   posts=new ArrayList<>();
        List<PostViewModel> viewModelList=new ArrayList<>();

        for(Post post:postList) {
            ResponseEntity<CommentVO[]> commentResponse = null;

            try {
                commentResponse = restTemplate.getForEntity(commentservice_url + "?postId=" + post.getId(), CommentVO[].class);
            } catch (Exception e) {
                logger.error("Exception in findAllPosts method while requesting comments");
            }

            List<Comment> comments = new ArrayList<>();
            List<Comment> commentList = new ArrayList<>();

            try {
                for (CommentVO commentVO : commentResponse.getBody()) {
                    Comment comment = new Comment(commentVO.getId());
                    mapper.map(commentVO, comment);
                    comments.add(comment);
                }
            } catch (Exception e){
                logger.error("Exception in findAllPosts method - Mapping comments");
            }

            try {
                for (Comment comment : comments) {
                    List<Comment> children = getChildComments(comment.getId());
                    for (Comment child : children) {
                        for (Comment c : comments) {
                            if (c.getId().equals(child.getId())) comment.getChildren().add(c);
                        }
                    }
                }
            }catch (Exception e){
                logger.error("Exception in findAllPosts method while adding children comments");
            }

            post.setComments(comments);

            viewModelList.add(new PostViewModel(post, post.getUser().getUsername(), new Date().toString()));
            posts.add(post);
        }



        model.addAttribute("posts",viewModelList);
        return "posts";
    }


    @RequestMapping(value="/addComment/{postId}",method = RequestMethod.POST)
    public String addComment(String text,
                             @PathVariable("postId")Integer postId,
                             @RequestParam(value = "commentId",required = false)Integer commentId,
                             Model model)throws Exception{

        RestTemplate restTemplate=new RestTemplate();

        CommentVO commentVO=new CommentVO();
        commentVO.setId(1);
        commentVO.setText(text);
        if(commentId!=null) commentVO.setParentId(commentId);
        commentVO.setPostId(postId);

        ResponseEntity<String> response=restTemplate.postForEntity(commentservice_url,commentVO,String.class);
        logger.info(response.getBody());

        return "redirect:/posts";
    }

    @RequestMapping(value="/deleteComment/{commentId}",method = RequestMethod.POST)
    public String deleteComment(@PathVariable("commentId")Integer commentId)    throws Exception{
        RestTemplate restTemplate=new RestTemplate();
        restTemplate.delete(commentservice_url+"/"+commentId);

        logger.info("Comment deleted");

        return "redirect:/posts";
    }

    @RequestMapping(value="/addPost",method = RequestMethod.GET)
    public String addPostForm(@ModelAttribute("post") PostVO postVO,
                              Model model)  throws Exception{

        RestTemplate restTemplate=new RestTemplate();

        ResponseEntity<UserVO[]> response=restTemplate.getForEntity(userservice_url,UserVO[].class);
        UserVO[] users =response.getBody();

        List<UserViewModel> userList=new ArrayList<>();

        for(UserVO user:users){
            userList.add(new UserViewModel(user.getId(),user.getUsername()));
        }

        model.addAttribute("users",userList);
        return "addPost";
    }

    @RequestMapping(value="/addPost",method = RequestMethod.POST)
    public String addPost(@ModelAttribute("post") PostVO postVO,
                          Model model){

        postVO.setId(1);
        List<String> errors=new ArrayList<>();

        if(postVO.getText()==null
        || postVO.getText().length()==0) errors.add("Post text cannot be null");

        if(postVO.getUserId()==null)    errors.add("User ID cannot be null");

        if(errors.size()>0) {
            model.addAttribute("errors", errors);
            return "addPost";
        }


        RestTemplate restTemplate=new RestTemplate();
        try {
            ResponseEntity<PostVO> response = restTemplate.postForEntity(postservice_url, postVO, PostVO.class);

            if(response.getStatusCode().equals(HttpStatus.CREATED)){
                return "redirect:/posts";
            }

        }catch (Exception e) {
            logger.error("Exception in addPost method while posting entity");
            return "addPost";
        }

        return "redirect:/posts";
    }

    @RequestMapping(value="/deletePost/{postId}",method = RequestMethod.POST)
    public String deletePost(@PathVariable("postId")Integer postId){

        RestTemplate restTemplate=new RestTemplate();

        try{
            restTemplate.delete(postservice_url+"/"+postId);
        }catch (Exception e){
            logger.error("Exception in deletePost method while deleting post");
        }

        try {
            restTemplate.delete(commentservice_url + "/post/" + postId + "/delete");
        }catch (Exception e){
            logger.error("Exception in deletePost method while deleting comments");
        }

        return "redirect:/posts";
    }

}


