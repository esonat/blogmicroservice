package com.example.blog.webapplication.valueobject;

import java.util.Date;

public class CommentVO {
    private Integer id;
    private Integer postId;
    private Integer parentId;
    private String text;
    private Date datetime;
    private Integer depth;

    public CommentVO(){
    }

    public CommentVO(Integer id, Integer postId, Integer parentId, String text, Date datetime,Integer depth) {
        this.id = id;
        this.postId = postId;
        this.parentId = parentId;
        this.text = text;
        this.datetime = datetime;
        this.depth=depth;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public Integer getDepth() {
        return depth;
    }

    public void setDepth(Integer depth) {
        this.depth = depth;
    }

    @Override
    public String toString() {
        return new StringBuilder("{id: ").append(id)
                .append(", postId: ").append(postId)
                .append(", parentId: ").append(parentId)
                .append(", text: ").append(text)
                .append(", datetime: ").append(datetime)
                .append(", depth: ").append(depth)
                .append("}").toString();
    }
}
