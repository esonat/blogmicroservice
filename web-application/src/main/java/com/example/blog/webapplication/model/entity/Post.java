package com.example.blog.webapplication.model.entity;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Post extends BaseEntity<Integer>{
    private String text;
    private Date datetime;
    private List<Comment> comments=new ArrayList<Comment>();
    private User user;

    public Post(Integer id){
        super(id);
    }

    public Post(Integer id,String text,User user,Date datetime,List<Comment> comments){
        super(id);
        this.text=text;
        this.user=user;
        this.datetime=datetime;
        this.comments=comments;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        return new
                StringBuilder("{id: ").append(id)
                .append(", text: ").append(text)
                .append(", user: ").append(user)
                .append(", comments: ").append(comments)
                .append("}").toString();
    }
}
