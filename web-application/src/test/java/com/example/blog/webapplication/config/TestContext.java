package com.example.blog.webapplication.config;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;

import java.util.Properties;

@Configuration
public class TestContext {

    @Bean
    PropertyPlaceholderConfigurer propConfig() {
        PropertyPlaceholderConfigurer placeholderConfigurer = new PropertyPlaceholderConfigurer();
        placeholderConfigurer.setLocation(new ClassPathResource("application.properties"));
        return placeholderConfigurer;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer properties() throws Exception{
        final PropertySourcesPlaceholderConfigurer configurer=new PropertySourcesPlaceholderConfigurer();
        Properties properties=new Properties();
        properties.setProperty("post-service.url","http://localhost:2222/v1/posts");
        properties.setProperty("comment-service.url","http://localhost:2225/v1/comments");
        properties.setProperty("user-service.url","http://localhost:2224/v1/users");

        configurer.setProperties(properties);
        return configurer;
    }
}
