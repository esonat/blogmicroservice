package com.example.blog.webapplication.controller;

import com.example.blog.webapplication.config.TestContext;
import com.example.blog.webapplication.config.WebAppContext;
import com.example.blog.webapplication.model.entity.User;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebAppContext.class, TestContext.class})
@WebAppConfiguration
public class DefaultControllerTest{

    private DefaultController defaultController;

    @Value("${post-service.url}")
    private String postservice_url;

    @Value("${comment-service.url}")
    private String commentservice_url;

    @Value("${user-service.url}")
    private String userservice_url;

//    @BeforeClass
//    public static void beforeClass(){
//        System.setProperty("user-service.url","http://localhost:2224/v1/users");
//
//    }
    @Before
    public void init(){
        defaultController=new DefaultController();
    }

    @Test
    public void Should_Return_user1_When_Get_User_And_Id_1() throws Exception{
        User user=defaultController.getUserFromUserId(1);
        Assert.assertEquals("user1",user.getUsername());
    }

    @Test
    public void Should_Return_user2_When_Get_User_And_Id_2() throws Exception{
        User user=defaultController.getUserFromUserId(2);
        Assert.assertEquals("user2",user.getUsername());
    }


//
//    @AfterClass
//    public static void afterClass(){
//        System.clearProperty("user-service.url");
//    }

}
