package com.example.blog.comment.resources;


import com.example.blog.comment.CommentApp;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CommentApp.class)
@WebIntegrationTest("server.port=0")
public class CommentControllerIntegrationTests
extends AbstractCommentControllerTests{

    private final RestTemplate restTemplate=new TestRestTemplate();
    public static final ObjectMapper objectMapper=new ObjectMapper();

    @Value("${local.server.port}")
    private int port;

    @Test
    public void testGetById(){
        Map<String,Object> response
                =restTemplate.getForObject("http://localhost:"+port+"/v1/comments/1",Map.class);

        Assert.assertNotNull(response);

        String id=response.get("id").toString();
        assertNotNull(id);
        assertEquals("1",id);
        String text=response.get("text").toString();
        assertNotNull(text);
        assertEquals("comment1",text);
    }

    @Test
    public void Should_Return_NoContent_When_Get_By_Id_And_Id_Invalid(){
        HttpHeaders headers=new HttpHeaders();
        HttpEntity<Object> entity=new HttpEntity<>(headers);
        ResponseEntity<Map> responseE=
                restTemplate.exchange("http://localhost:"+port+"/v1/comments/99", HttpMethod.GET,entity,Map.class);

        assertNotNull(responseE);
        assertEquals(HttpStatus.NO_CONTENT,responseE.getStatusCode());
    }

    @Test
    public void testGetByPostId(){
        HttpHeaders headers=new HttpHeaders();
        HttpEntity<Object> entity=new HttpEntity<>(headers);
        Map<String,Object> uriVariables=new HashMap<>();

        uriVariables.put("postId",1);

        ResponseEntity<Map[]> responseE=restTemplate.exchange("http://localhost:"+port+"/v1/comments?postId={postId}",
                HttpMethod.GET,entity,Map[].class,uriVariables);

        assertNotNull(responseE);
        assertEquals(HttpStatus.OK,responseE.getStatusCode());
        Map<String,Object>[] responses=responseE.getBody();


        System.out.println("Length:"+responses.length);

        Map<String,Object> response=responses[0];
        String id=response.get("id").toString();
        assertNotNull(id);
        assertEquals("1",id);

        String text=response.get("text").toString();
        assertNotNull(text);
        assertEquals("comment1",text);
    }

    @Test
    public void testAdd() throws JsonProcessingException{
        Map<String,Object> requestBody=new HashMap<>();
        requestBody.put("id","9");
        requestBody.put("postId","1");
        requestBody.put("parentId","1");
        requestBody.put("text","comment10");
        requestBody.put("depth","0");

        HttpHeaders headers=new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity=new HttpEntity<>(objectMapper.writeValueAsString(requestBody),headers);

        ResponseEntity<Map> responseE=restTemplate.exchange("http://localhost:"+port+"/v1/comments",HttpMethod.POST,
                entity,Map.class, Collections.EMPTY_MAP);

        assertNotNull(responseE);
        assertEquals(HttpStatus.CREATED,responseE.getStatusCode());

        Map<String,Object> response
                =restTemplate.getForObject("http://localhost:"+port+"/v1/comments/9",Map.class);
        assertNotNull(response);

        String id=response.get("id").toString();
        assertNotNull(id);
        assertEquals("9",id);
        String text=response.get("text").toString();
        assertNotNull(text);
        assertEquals("comment10",text);
    }

}
