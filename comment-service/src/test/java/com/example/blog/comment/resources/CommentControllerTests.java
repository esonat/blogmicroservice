package com.example.blog.comment.resources;

import com.example.blog.comment.domain.model.entity.Comment;
import com.example.blog.comment.domain.model.entity.Entity;
import com.example.blog.comment.domain.repository.CommentRepository;
import com.example.blog.comment.domain.service.CommentService;
import com.example.blog.comment.domain.service.CommentServiceImpl;
import org.junit.Before;

import java.util.*;

public class CommentControllerTests extends AbstractCommentControllerTests{

    protected static class TestCommentRepository implements CommentRepository<Comment,Integer>{
        private Map<Integer,Comment> comments;

        public TestCommentRepository(){
            comments=new HashMap<>();
            Comment comment=new Comment(COMMENT,POST_ID,null,COMMENT_TEXT,new Date(),0);
            comments.put(1,comment);
            comment=new Comment(2,1,1,"comment2",new Date(),1);
            comments.put(2,comment);
        }

        @Override
        public boolean containsText(String text){
            return comments.values().stream().anyMatch(m->m.getText().equals(text));
        }

        /**
         *
         * @param entity
         */
        @Override
        public void add(Comment entity) {
            comments.put(entity.getId(), entity);
        }

        /**
         *
         * @param id
         */
        @Override
        public void remove(Integer id) {
            if (comments.containsKey(id)) {
                comments.remove(id);
            }
        }

        /**
         *
         * @param entity
         */
        @Override
        public void update(Comment entity) {
            if (comments.containsKey(entity.getId())) {
                comments.put(entity.getId(), entity);
            }
        }

        /**
         *
         * @param id
         * @return
         */
        @Override
        public boolean contains(Integer id) {
            return comments.containsKey(id);
        }

        /**
         *
         * @param id
         * @return
         */
        @Override
        public Entity get(Integer id) {
            return comments.get(id);
        }

        /**
         *
         * @return
         */
        @Override
        public Collection<Comment> getAll() {
            return comments.values();
        }

        @Override
        public List<Comment> findByParentId(Integer parentId) throws Exception{
            List<Comment> items=new ArrayList<>();

            for(Comment comment:comments.values()){
                if(comment.getParentId()!=null &&
                        comment.getParentId()==parentId) items.add(comment);
            }

            return items;
        }

        @Override
        public Collection<Comment> findByPostId(Integer postId) throws Exception {
            Collection<Comment> items= new ArrayList();

            for(Comment comment:comments.values()){
                if(comment.getPostId()==postId) items.add(comment);
            }
            return items;
        }

        @Override
        public Comment findByText(String text) throws Exception {
            for(Comment comment:comments.values()){
                if(comment.getText().equals(text)) return comment;
            }
            return null;
        }

        @Override
        public Integer findLastId() throws Exception {
            return comments.size()+1;
        }

        @Override
        public Integer getDepth(Integer id) throws Exception {
            Comment comment=comments.get(id);
            return comment.getDepth();
        }

        @Override
        public Comment findById(Integer id) throws Exception {
            return comments.get(id);
        }
    }

    protected TestCommentRepository testCommentRepository=new TestCommentRepository();

    protected CommentService commentService=new CommentServiceImpl(testCommentRepository);

    @Before
    public void setup(){
        commentController=new CommentController(commentService);
    }
}
