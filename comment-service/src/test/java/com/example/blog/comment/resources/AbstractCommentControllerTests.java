package com.example.blog.comment.resources;

import com.example.blog.comment.domain.model.entity.Comment;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

public abstract class AbstractCommentControllerTests {

    protected static final Integer COMMENT=1;
    protected static final String COMMENT_TEXT="comment1";
    protected static final Integer POST_ID=1;

    @Autowired
    CommentController commentController;


    @Test
    public void validCommentById(){
        ResponseEntity<Comment> comment=commentController.findById(COMMENT);

        Assert.assertEquals(HttpStatus.OK,comment.getStatusCode());
        Assert.assertTrue(comment.hasBody());
        Assert.assertNotNull(comment.getBody());
        Assert.assertEquals(COMMENT,comment.getBody().getId());
        Assert.assertEquals(COMMENT_TEXT,comment.getBody().getText());
    }

    @Test
    public void validCommentsByPostId(){
        ResponseEntity<List<Comment>> response=commentController.findByPostId(POST_ID);

    }

}
