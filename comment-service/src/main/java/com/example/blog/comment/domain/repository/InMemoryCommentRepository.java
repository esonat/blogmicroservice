package com.example.blog.comment.domain.repository;

import com.example.blog.comment.domain.model.entity.Comment;
import com.example.blog.comment.domain.model.entity.Entity;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository("commentRepository")
public class InMemoryCommentRepository implements CommentRepository<Comment,Integer>{
    private Map<Integer,Comment> comments;

    public InMemoryCommentRepository(){
        comments=new HashMap<>();
        Comment comment=new Comment(1,1,null,"comment1",new Date(),0);
        Comment comment2=new Comment(2,1,1,"comment2",new Date(),1);
        Comment comment3=new Comment(3,1,2,"comment3",new Date(),2);
        Comment comment4=new Comment(4,1,1,"comment4",new Date(),1);
        Comment comment5=new Comment(5,1,2,"comment5",new Date(),2);
        Comment comment6=new Comment(6,1,5,"comment6",new Date(),3);
        Comment comment7=new Comment(7,1,6,"comment7",new Date(),4);
        Comment comment8=new Comment(8,1,1,"comment8",new Date(),1);


        comments.put(1,comment);
        comments.put(2,comment2);
        comments.put(3,comment3);
        comments.put(4,comment4);
        comments.put(5,comment5);
        comments.put(6,comment6);
        comments.put(7,comment7);
        comments.put(8,comment8);
    }

    @Override
    public boolean containsText(String text){
        return comments.values().stream().anyMatch(m->m.getText().equals(text));
    }

    /**
     *
     * @param entity
     */
    @Override
    public void add(Comment entity) {
        comments.put(entity.getId(), entity);
    }

    /**
     *
     * @param id
     */
    @Override
    public void remove(Integer id) {
        if (comments.containsKey(id)) {
            comments.remove(id);
        }
    }

    /**
     *
     * @param entity
     */
    @Override
    public void update(Comment entity) {
        if (comments.containsKey(entity.getId())) {
            comments.put(entity.getId(), entity);
        }
    }

    /**
     *
     * @param id
     * @return
     */
    @Override
    public boolean contains(Integer id) {
        return comments.containsKey(id);
    }

    /**
     *
     * @param id
     * @return
     */
    @Override
    public Entity get(Integer id) {
        return comments.get(id);
    }

    /**
     *
     * @return
     */
    @Override
    public Collection<Comment> getAll() {
        return comments.values();
    }

    @Override
    public List<Comment> findByParentId(Integer parentId) throws Exception{
        List<Comment> items=new ArrayList<>();

        for(Comment comment:comments.values()){
            if(comment.getParentId()!=null &&
                    comment.getParentId()==parentId) items.add(comment);
        }

        return items;
    }

    @Override
    public Collection<Comment> findByPostId(Integer postId) throws Exception {
        Collection<Comment> items= new ArrayList();

        for(Comment comment:comments.values()){
            if(comment.getPostId()==postId) items.add(comment);
        }
        return items;
    }

    @Override
    public Comment findByText(String text) throws Exception {
        for(Comment comment:comments.values()){
            if(comment.getText().equals(text)) return comment;
        }
        return null;
    }

    @Override
    public Integer findLastId() throws Exception {
        return comments.size()+1;
    }

    @Override
    public Integer getDepth(Integer id) throws Exception {
        Comment comment=comments.get(id);
        return comment.getDepth();
    }

    @Override
    public Comment findById(Integer id) throws Exception {
        return comments.get(id);
    }
}
