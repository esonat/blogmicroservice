package com.example.blog.comment.domain.service;

import com.example.blog.comment.domain.model.entity.Comment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface CommentService {

    void add(Comment comment) throws Exception;

    void update(Comment comment) throws Exception;

    void delete(Integer id) throws Exception;

    Comment findById(Integer id) throws Exception;

    Comment findByText(String text) throws Exception;

    Collection<Comment> findByPostId(Integer postId) throws Exception;

    List<Comment> findByParentId(Integer parentId) throws Exception;

    Collection<Comment> findByCriteria(Map<String, ArrayList<String>> name) throws Exception;

    Integer findLastId() throws Exception;

    Integer getDepth(Integer id) throws Exception;
}