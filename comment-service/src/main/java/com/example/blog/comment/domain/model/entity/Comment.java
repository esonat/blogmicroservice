package com.example.blog.comment.domain.model.entity;

import java.util.Date;

public class Comment extends BaseEntity<Integer> {
    private Integer postId;
    private Integer parentId;
    private String text;
    private Date datetime;
    private Integer depth;

    public Comment(Integer id){
        super(id);
    }
    public Comment(Integer id, Integer postId, Integer parentId, String text, Date datetime,Integer depth) {
        super(id);
        this.postId = postId;
        this.parentId = parentId;
        this.text = text;
        this.datetime = datetime;
        this.depth=depth;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public Integer getDepth() {
        return depth;
    }

    public void setDepth(Integer depth) {
        this.depth = depth;
    }

    @Override
    public String toString() {
        return new StringBuilder("{id: ").append(id)
                .append(", postId: ").append(postId)
                .append(", parentId: ").append(parentId)
                .append(", text: ").append(text)
                .append(", datetime: ").append(datetime)
                .append(", depth: ").append(depth)
                .append("}").toString();
    }
}
