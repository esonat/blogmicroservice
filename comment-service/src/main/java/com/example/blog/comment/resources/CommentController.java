package com.example.blog.comment.resources;


import com.example.blog.comment.domain.model.entity.Comment;
import com.example.blog.comment.domain.service.CommentService;
import com.example.blog.comment.domain.valueobject.CommentVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/v1/comments")
public class CommentController {

    Logger logger = LoggerFactory.getLogger(CommentController.class);

    protected CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }


    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Comment>> findByPostId(@RequestParam(value = "postId", required = false) Integer postId) {
        List<Comment> comments = new ArrayList<>();
        logger.info("Requested findByPostId method");

        try {
            comments.addAll(commentService.findByPostId(postId));
        } catch (Exception e) {
            logger.error("Exception in findByPostId");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return comments != null ? new ResponseEntity<>(comments, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/{comment_id}", method = RequestMethod.GET)
    public ResponseEntity<Comment> findById(@PathVariable("comment_id") Integer id) {
        Comment comment;

        try {
            comment = commentService.findById(id);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return comment != null ? new ResponseEntity<>(comment, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/{comment_id}/children", method = RequestMethod.GET)
    public ResponseEntity<List<Comment>> findChildrenComments(@PathVariable("comment_id") Integer id) {
        List<Comment> children;
        try {
            children = commentService.findByParentId(id);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return children != null ? new ResponseEntity<>(children, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> add(@RequestBody CommentVO commentVO) {


        if (commentVO.getText() == null) {
            logger.error("Text cannot be null");
            return new ResponseEntity("Text cannot be null", HttpStatus.UNPROCESSABLE_ENTITY);
        }
        if (commentVO.getPostId() == null) {
            logger.error("Post ID cannot be null");
            return new ResponseEntity("Post ID cannot be null", HttpStatus.UNPROCESSABLE_ENTITY);
        }

        logger.info("Requested add method: " + commentVO.toString());
        Integer ID = new Integer(1);

        try {

            ID = commentService.findLastId();
        } catch (Exception e) {
            logger.error("Exception in findLastID");
            return new ResponseEntity<>("Exception in findLastID", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        Comment comment = new Comment(ID);
        comment.setDatetime(new Date());
        comment.setText(commentVO.getText());
        comment.setPostId(commentVO.getPostId());

        Integer depth = new Integer(0);

        if (commentVO.getParentId() != null) {
            comment.setParentId(commentVO.getParentId());

            try {
                depth = commentService.getDepth(commentVO.getParentId()) + 1;
            } catch (Exception e) {
                logger.error("Exception in getDepth() method");
                return new ResponseEntity<>("Exception in getDepth() method", HttpStatus.INTERNAL_SERVER_ERROR);
                //return "Exception in getDepth() method";
            }
        } else {
            comment.setParentId(null);
        }

        comment.setDepth(depth);

        try {
            commentService.add(comment);

        } catch (Exception e) {
            logger.error("Exception in add comment");
            return new ResponseEntity("Exception in add() method", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    public void deleteChildren(Integer parentId) {
        try {
            commentService.delete(parentId);
            List<Comment> children = commentService.findByParentId(parentId);

            if (children == null || children.size() == 0) return;

            for (Comment comment : children) {
                deleteChildren(comment.getId());
            }

        } catch (Exception e) {
            logger.error("Exception in deleteChildren method");
        }
    }


    @RequestMapping(value = "/{commentId}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteComment(@PathVariable("commentId") Integer commentId) {
        try {

            deleteChildren(commentId);

        } catch (Exception e) {
            logger.error("Exception while deleting comment");
            return new ResponseEntity<String>("Exception while deleting comment", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<String>("Comment deleted", HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/post/{postId}/delete", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteCommentsOfPost(@PathVariable("postId")Integer postId){
        try {
            for (Comment comment : commentService.findByPostId(postId)){
                commentService.delete(comment.getId());
            }
        }catch (Exception e){
            logger.error("Exception in deleteCommentsOfPost method");
            return new ResponseEntity<>("Exception in deleteCommentsOfPost method",HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>("Deleted comments of post",HttpStatus.NO_CONTENT);
    }
}
