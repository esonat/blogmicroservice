package com.example.blog.comment.domain.repository;

import java.util.Collection;
import java.util.List;

public interface CommentRepository<Comment,Integer> extends Repository<Comment,Integer>{

    boolean containsText(String text);

    Comment findByText(String text) throws Exception;

    List<Comment> findByParentId(Integer parentId) throws Exception;

    Collection<Comment> findByPostId(Integer postId) throws Exception;

    Integer findLastId() throws Exception;

    Comment findById(Integer id) throws Exception;

    Integer getDepth(Integer id) throws Exception;
}
