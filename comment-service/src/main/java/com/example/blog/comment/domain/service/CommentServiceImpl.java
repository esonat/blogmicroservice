package com.example.blog.comment.domain.service;

import com.example.blog.comment.domain.model.entity.Comment;
import com.example.blog.comment.domain.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Service("commentService")
public class CommentServiceImpl extends BaseService<Comment,Integer>
 implements CommentService{
    private CommentRepository<Comment,Integer> commentRepository;

    @Autowired
    public CommentServiceImpl(CommentRepository<Comment,Integer> commentRepository) {
        super(commentRepository);
        this.commentRepository = commentRepository;
    }

    @Override
    public void add(Comment comment) throws Exception{
//        if(commentRepository.containsText(comment.getText())){
//            throw new Exception(String.format("There is already a comment  with the same text"));
//        }
//        if(comment.getText()==null || "".equals(comment.getText())){
//            throw new Exception("Comment text cannot be null or empty");
//        }
//
        commentRepository.add(comment);
    }

    @Override
    public Comment findByText(String text) throws Exception{
        return commentRepository.findByText(text);
    }

    @Override
    public void update(Comment comment) throws Exception{
        commentRepository.update(comment);
    }

    @Override
    public void delete(Integer id) throws Exception{
        commentRepository.remove(id);
    }

    @Override
    public Comment findById(Integer id) throws Exception{
        return commentRepository.findById(id);
    }

    @Override
    public Collection<Comment> findByCriteria(Map<String, ArrayList<String>> name) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Collection<Comment> findByPostId(Integer postId) throws Exception {
        return commentRepository.findByPostId(postId);
    }

    @Override
    public List<Comment> findByParentId(Integer parentId) throws Exception {
        //List<Comment> list=new ArrayList<>();
        ///list.addAll(commentRepository.findByParentId(parentId));
        return commentRepository.findByParentId(parentId);
    }

    @Override
    public Integer findLastId() throws Exception {
        return commentRepository.findLastId();
    }

    @Override
    public Integer getDepth(Integer id) throws Exception {
        return commentRepository.getDepth(id);
    }
}
